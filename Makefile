# =============================================================================
# Makefile variables
# =============================================================================
VERSION=1.0.0
TAG=v$(VERSION)
IMAGE_NAME=todos-mysql
IMAGE_REPO=quay.io/corbsmartin/$(IMAGE_NAME)
# =============================================================================
# Makefile targets
# =============================================================================
build:
	@mvn versions:set -DnewVersion=$(VERSION)
	@mvn clean package -DskipTests=true

image:
	@podman build --no-cache -t $(IMAGE_NAME):$(TAG) --build-arg=APP_VERSION=$(VERSION) .

push: image
	@podman tag $(IMAGE_NAME):$(TAG) $(IMAGE_REPO):$(TAG)
	@podman push $(IMAGE_REPO):$(TAG)
